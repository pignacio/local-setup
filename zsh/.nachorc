# Virtualenv stuff
WORKON_HOME=/home/ignacio/.virtualenvs
[[ -f /etc/bash_completion.d/virtualenvwrapper ]] && source /etc/bash_completion.d/virtualenvwrapper

# Gets python binary from a virtualenv, or default python nif it does not exist
function vefile() {
  ve_python="/home/ignacio/.virtualenvs/$1/$2"
  if [[ -f "$ve_python" ]]; then
    echo $ve_python;
  else
    return 1
  fi
}

function vepy() {
  if ! vefile $1 bin/python; then
    echo python
  fi
}

function vepip() {
  if ! vefile $1 bin/pip; then
    echo pip
  fi
}


alias apt-get='sudo apt-get'
alias mee='mvn eclipse:eclipse'


alias prmb='git checkout master; git pull; rmb'
alias gk='gitk --all'
alias 'gpf!'='git push --force'
alias grb='git rebase'
alias grbi='git rebase -i'
alias gpt='gp; gp --tags'
alias gap='git add --patch'

alias aptana='nohup ~/opt/aptana/AptanaStudio3 >/dev/null 2>&1 &'
alias chromium='nohup chromium-browser >>~/.logs/chromium.out 2>>~/.logs/chromium.err &'
alias incognito='nohup chromium-browser --incognito >>/dev/null 2>&1 &'
alias hh-chromium='nohup chromium-browser --user-data-dir=/home/ignacio/.hotelhoje/chromium >>/home/ignacio/.hotelhoje/logs/chromium.out 2>>/home/ignacio/.hotelhoje/logs/chromium.err &'

alias phpcheck='for x in $(find -name "*.php"); do php -l $x; done'

source $HOME/.gitrmb

alias pylint='pylint -f colorized -i y -d C0111'

alias tomb='sudo tomb'
alias gifmaker="$(vepy gifmaker) /home/ignacio/workspaces/aptana/gifmaker/src/gifmaker.py"
alias downchan="$(vepy downchan) /home/ignacio/workspaces/aptana/downchan/src/downchan.py"
alias beet-playlist="$(vepy beet-playlist) /home/ignacio/workspaces/aptana/beet-playlist/src/main.py"
alias bp='beet-playlist'
alias chorddb="$(vepy chorddb) /home/ignacio/workspaces/aptana/chorddb/src/chorddb.py"
alias issue-branch="$(vepy issue-branch) /home/ignacio/workspaces/aptana/scripts/src/issue_branch/issue_branch.py"
alias p8='find -name "*.py" -exec pep8 {} \;'
alias ap8='find -name "*.py" -exec autopep8 -i {} \;'

if [[ ! -z "$(vefile youtube-dl bin/youtube-dl)" ]]; then
  alias youtube-dl="$(vepy youtube-dl) $(vefile youtube-dl bin/youtube-dl)"
fi

# play all function
pall() {
  for arg; do
    if [ "$arg" = "--random" ]; then
      SORT_FLAGS="${SORT_FLAGS:-} -R"
    fi
  done
  noglob find $1 -name *.mp3 -o -name *.flac -o -name *.ogg | sort ${SORT_FLAGS:-} | tr '\n' '\0' | xargs -0 -n 10000 mplayer
}

#puppet stuff
alias puppet-lint-all='noglob find -name *.pp -exec puppet-lint --with-filename {} +;'
alias pla=puppet-lint-all

# get the name of the branch we are on
function git_prompt_info() {
  if [[ "$(git config --get oh-my-zsh.hide-status)" != "1" ]]; then
    ref=$(command git symbolic-ref HEAD 2> /dev/null) || \
    ref=$(command git rev-parse --short HEAD 2> /dev/null) || return
    ref=${ref#refs/heads/}
    if [[ ${#ref} -gt 30 ]]; then
      ref="$(echo $ref | cut -c -27)..."
    fi
    echo "$ZSH_THEME_GIT_PROMPT_PREFIX${ref}$(parse_git_dirty)$ZSH_THEME_GIT_PROMPT_SUFFIX"
  fi
}

# vim: ft=sh
